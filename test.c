#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"


float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	float TEMPERATURA[10];
	float HUMEDAD[10];
	int i=0;
	float resulTemp=0;
	float resulHume=0;

	char on='1';
	char off='0';
	char a='t';
	char b='h';
	
	int fd = -1;
	int baudrate = 9600;  // default

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
        
        serialport_flush(fd);
		
	
        while(i<10){
		usleep(5000);
		write(fd,&on,1);
		write(fd,&a,1);
		usleep(5000);
		int tempe=0;
        	int p =read(fd, &tempe, 1);
        	
		while(p==0){
			p=read(fd,&tempe,1);}
		TEMPERATURA[i]=(float)tempe;
		printf("TEMPERATURA: %f\n",TEMPERATURA[i]);
		
		write(fd,&b,1);
		usleep(5000);
		
		int humedad=0;
		p=read(fd,&humedad,1);
		while(p==0){
			p=read(fd,&humedad,1);}
		
		HUMEDAD[i]=(float)humedad;
		printf("HUMEDAD: %f\n",HUMEDAD[i]);
		write(fd,&off,1);
		i=i+1;
	}
	
	resulTemp=calculateSD(TEMPERATURA);
	resulHume=calculateSD(HUMEDAD);
	printf("Desviacion estandar TEMPERATURA: %.2f\n",resulTemp);
	printf("Desviacion estandar HUMEDAD: %.2f\n",resulHume);



	close( fd );
	return 0;	

    
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}

